import { GET_OFFICE, PUT_OFFICE } from "./constants";

export const getOffice = () => ({
  type: GET_OFFICE,
});

export const putOffice = (office: any) => ({
  type: PUT_OFFICE,
  office,
});
