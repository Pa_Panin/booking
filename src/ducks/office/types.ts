export interface IOfficeState {
  loading: boolean;
  allOffice: IOfficeType[];
}

export type IOfficeType = {
  id: string;
  name: string;
  createdAt: string;
  updatedAt: string;
  chatrooms: IOfficeChartroom[];
};

export type IOfficeChartroom = {
  id: string;
  name: string;
  createdAt: string;
  updatedAt: string;
};
