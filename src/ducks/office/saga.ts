import { takeLatest, put, call, all } from "redux-saga/effects";

import { OfficeServices } from "../../services";

import { putOffice } from "./action";

import { GET_OFFICE } from "./constants";

function* workerGetOffice(): Generator {
  const initMoviesServices = OfficeServices.getOffice;
  try {
    const data = yield call(initMoviesServices);
    yield put(putOffice(data));
  } catch (error) {
    alert("Error");
  }
}

const offceSaga = function* () {
  yield all([takeLatest(GET_OFFICE, workerGetOffice)]);
};

export default offceSaga;
