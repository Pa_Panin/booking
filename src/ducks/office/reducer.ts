import { IOfficeState } from ".";
import { GET_OFFICE, PUT_OFFICE } from "./constants";

const initialState: IOfficeState = {
  loading: false,
  allOffice: [
    {
      id: "",
      name: "",
      createdAt: "",
      updatedAt: "",
      chatrooms: [
        {
          id: "",
          name: "",
          createdAt: "",
          updatedAt: "",
        },
      ],
    },
  ],
};

const Reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_OFFICE:
      return {
        ...state,
        loading: false,
      };
    case PUT_OFFICE:
      return {
        ...state,
        loading: true,
        allOffice: action.office.data.payload,
      };
    default:
      return state;
  }
};

export default Reducer;
