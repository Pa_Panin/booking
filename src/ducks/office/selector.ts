import { createSelector } from "reselect";

import { IState } from "../stateTypes";

const stateOffice = (state: IState) => state.office;

export const getAllOffice = createSelector(
  stateOffice,
  (stateOffice) => stateOffice.allOffice
);

export const getLoadingOffice = createSelector(
  stateOffice,
  (stateOffice) => stateOffice.loading
);
