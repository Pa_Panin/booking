import { all, fork } from "redux-saga/effects";
import { combineReducers } from "redux";

import officeReducer, {
  saga as officeSaga,
  actions as officeAction,
  selectors as officeSelectors,
} from "./office";

import bookingReducer, {
  saga as bookingSaga,
  actions as bookingAction,
  selectors as bookingSelectors,
} from "./booking";

const rootReducer = combineReducers({
  office: officeReducer,
  booking: bookingReducer,
});

const actions = {
  office: officeAction,
  booking: bookingAction,
};

const selectors = {
  office: officeSelectors,
  booking: bookingSelectors,
};

function* rootSaga() {
  yield all([fork(officeSaga)]);
  yield all([fork(bookingSaga)]);
}

export {
  rootReducer as default,
  rootSaga,
  selectors as Selectors,
  actions as Actions,
};
