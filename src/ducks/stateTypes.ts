import { IBookingState } from "./booking";
import { IOfficeState } from "./office";

export interface IState {
  office: IOfficeState;
  booking: IBookingState;
}
