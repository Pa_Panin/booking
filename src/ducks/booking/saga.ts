import { takeLatest, put, call, all } from "redux-saga/effects";

import { BookingSevices } from "../../services";

import { putBooking, putPostBooking } from "./action";

import {
  GET_BOOKING,
  PATCH_BOOKING,
  POST_BOOKING,
  DELETE_BOOKING,
  GET_BOOKING_CHATROOMS,
} from "./constants";

function* workerGetBooking(action: any): Generator<any> {
  const initBookingServices = BookingSevices.getBooking;
  try {
    const data = yield call(() => initBookingServices(action.payload));
    yield put(putBooking(data));
  } catch (error) {
    alert("Error");
  }
}

function* workerGetBookingChatrooms(action: any): Generator<any> {
  const initBookingServices = BookingSevices.getBookingChatrooms;
  try {
    const data: any = yield call(() => initBookingServices(action.payload));
    yield put(putBooking(data));
  } catch (error) {
    alert("Error");
  }
}

function* workerPostBooking(action: any): Generator<any> {
  const initBookingServices = BookingSevices.postBooking;
  try {
    const post = yield call(() => initBookingServices(action.booking));
    yield put(putPostBooking(post));
  } catch (error) {
    alert("Error");
  }
}

function* workerDeleteBooking(data: any): Generator<any> {
  const initBookingServices = BookingSevices.deleteBooking;
  try {
    yield call(() => initBookingServices(data));
  } catch (error) {
    alert("Error");
  }
}

function* workerPatchBooking(data: any): Generator<any> {
  const initBookingServices = BookingSevices.patchBooking;
  try {
    yield call(() => initBookingServices(data.payload));
  } catch (error) {
    alert("Error");
  }
}

const bookingSaga = function* () {
  yield all([takeLatest(GET_BOOKING, workerGetBooking)]);
  yield all([takeLatest(GET_BOOKING_CHATROOMS, workerGetBookingChatrooms)]);
  yield all([takeLatest(POST_BOOKING, workerPostBooking)]);
  yield all([takeLatest(DELETE_BOOKING, workerDeleteBooking)]);
  yield all([takeLatest(PATCH_BOOKING, workerPatchBooking)]);
};

export default bookingSaga;
