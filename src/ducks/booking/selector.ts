import { createSelector } from "reselect";

import { IState } from "../stateTypes";

const stateBooking = (state: IState) => state.booking;

export const getAllBooking = createSelector(
  stateBooking,
  (stateBooking) => stateBooking.allBooking
);

export const getLoadingBooking = createSelector(
  stateBooking,
  (stateBooking) => stateBooking.loading
);
