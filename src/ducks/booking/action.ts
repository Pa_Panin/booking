import {
  GET_BOOKING,
  GET_BOOKING_CHATROOMS,
  PUT_BOOKING,
  POST_BOOKING,
  PATCH_BOOKING,
  DELETE_BOOKING,
  PUT_POST_BOOKING,
} from "./constants";

export const getBooking = (roomName: { roomName: string }) => ({
  type: GET_BOOKING,
  payload: roomName,
});

export const getBookingChatrooms = (roomName: {
  roomName: string;
  reject: () => void;
}) => ({
  type: GET_BOOKING_CHATROOMS,
  payload: roomName,
});

export const putBooking = (booking: any) => ({
  type: PUT_BOOKING,
  booking,
});

export const postBooking = (booking: any) => ({
  type: POST_BOOKING,
  booking,
});

export const putPostBooking = (booking: any) => ({
  type: PUT_POST_BOOKING,
  booking,
});

export const deleteBooking = (data: string) => ({
  type: DELETE_BOOKING,
  data,
});

export const patchBooking = (data: any) => ({
  type: PATCH_BOOKING,
  payload: data,
});
