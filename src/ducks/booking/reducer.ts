import moment from "moment";

import { IBookingState, IBookingType } from ".";
import {
  GET_BOOKING,
  PUT_BOOKING,
  PATCH_BOOKING,
  POST_BOOKING,
  DELETE_BOOKING,
  PUT_POST_BOOKING,
} from "./constants";

const initialState: IBookingState = {
  loading: false,
  allBooking: [
    {
      title: "",
      from: "",
      to: "",
      id: "",
      chatroom: {
        id: "",
      },
    },
  ],
};

const dateFix = (item: IBookingType) => {
  const newItem = {
    from: moment(item.from).toDate().toLocaleString(),
    to: moment(item.to).toDate().toLocaleString(),
    title: item.title,
    id: item.id,
  };
  return newItem;
};

const Reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case GET_BOOKING:
      return {
        ...state,
        loading: false,
      };
    case PUT_BOOKING:
      return {
        ...state,
        loading: true,
        allBooking: action.booking.data.payload.bookings.map(
          (el: IBookingType) => dateFix(el)
        ),
      };
    case POST_BOOKING:
      return {
        ...state,
        loading: false,
      };
    case PUT_POST_BOOKING:
      return {
        ...state,
        loading: true,
        allBooking: [...state.allBooking, dateFix(action.booking.data.payload)],
      };
    case PATCH_BOOKING:
      const newEl = state.allBooking.map((el) => {
        if (el.id === action.payload.data.id) {
          return dateFix({
            from: el.from,
            to: el.to,
            title: action.payload.data.title,
            id: action.payload.data.id,
            chatroom: action.payload.data.chatroom,
          });
        }
        return el;
      });
      return {
        ...state,
        loading: true,
        allBooking: newEl,
      };
    case DELETE_BOOKING:
      const b = state.allBooking.filter((el) => el.id !== action.data);

      return {
        ...state,
        loading: true,
        allBooking: b,
      };
    default:
      return state;
  }
};

export default Reducer;
