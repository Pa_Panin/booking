export interface IBookingState {
  loading: boolean;
  allBooking: IBookingType[];
}

export type IBookingType = {
  title: string;
  from: string;
  to: string;
  id: string;
  chatroom: {
    id: string;
  };
};
