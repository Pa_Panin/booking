import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { MainPage, NoMatch } from "./pages";

import { configureStore } from "./store";

import "./index.css";

const { store } = configureStore();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <link
        href="https://fonts.googleapis.com/css?family=IBM Plex Sans"
        rel="stylesheet"
      />
      {/* скачать шрифт и подключить локально */}
      <Router>
        <Switch>
          <Route exact path="/office/:officeName/room/:roomName">
            <MainPage />
          </Route>
          <Route>
            <NoMatch />
          </Route>
        </Switch>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
