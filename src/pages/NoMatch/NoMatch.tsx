import React, { useEffect } from "react";

import { useHistory } from "react-router";

import { useDispatch, useSelector } from "react-redux";

import { Actions, Selectors } from "../../ducks";

const NoMatch = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const allOffices = useSelector(Selectors.office.getAllOffice);

  const getOfficeApi = () => {
    dispatch(Actions.office.getOffice());
  };

  useEffect(() => {
    getOfficeApi();
  }, []);

  if (allOffices.length >= 1) {
    history.replace(``);
    history.push(
      `office/${allOffices[0].id}/room/${allOffices[0].chatrooms[0].id}`
    );
  }

  return null;
};

export default NoMatch;
