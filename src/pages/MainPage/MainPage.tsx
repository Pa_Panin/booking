import React, { useEffect, useRef, useState } from "react";

import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";

import { Actions, Selectors } from "../../ducks";

import { Header, TimePade } from "../../components";
import { InfoPade } from "../../components/InfoPade";
import { debouns, oneStepInMin, timeInMinNow } from "../../utils";

import { Modal } from "../../components/Modal";

import styles from "./styles.module.scss";

const MainPage: React.FC = () => {
  const dispatch = useDispatch();

  const timeToBooking = moment();

  const scrollRef = useRef<HTMLDivElement>(null);

  const [openModal, setOpenModal] = useState({ flag: false, id: "" });
  const [addBookingFlag, setAddBookingFlag] = useState(true);
  const [changeBooking, setChangeBooking] = useState({ flag: true, id: "" });
  const [time, setTime] = useState("00:00");
  const [newBooking, setNewBooking] = useState({
    name: "",
    houre: `${timeToBooking.format("HH")}`,
    minute: `${+timeToBooking.format("mm").substr(0, 1) + 1}0`,
    duration: "10",
  });

  const allOffices = useSelector(Selectors.office.getAllOffice);
  const loadingOffice = useSelector(Selectors.office.getLoadingOffice);

  const { roomName }: { roomName: string } = useParams();

  useEffect(() => {
    if (+timeToBooking.format("mm") >= 50) {
      setNewBooking({
        ...newBooking,
        houre: `${+newBooking.houre + 1}`,
        minute: "00",
      });
    }
  }, []);

  const [activeTimePade, setActiveTimePade] = useState({
    active: "none",
    top: "5",
    height: "100",
  });

  const getOfficeApi = () => {
    dispatch(Actions.office.getOffice());
  };
  const history = useHistory();

  const goToNoMatch = () => {
    history.push("");
  };

  const dateTime = () => {
    const time = moment(new Date());
    return time;
  };

  useEffect(() => {
    getOfficeApi();
  }, []);

  let debounceScroll = () => {
    console.log("new");
    if (scrollRef.current !== null) {
      scrollRef.current.scrollTo(0, timeInMinNow(time) * oneStepInMin - 250);
    }
  };
  const moveMouse = () => {
    debounceScroll = debouns(debounceScroll, 2000);
  };

  return (
    <div className={styles.main_container} onMouseMove={moveMouse}>
      <div>
        <Modal
          openModal={openModal}
          newBooking={newBooking}
          changeBooking={changeBooking}
          setOpenModal={setOpenModal}
          setChangeBooking={setChangeBooking}
        />
        {loadingOffice ? (
          <Header
            officeProps={allOffices.map((el) => {
              return { name: el.name, id: el.id, chatrooms: el.chatrooms };
            })}
            goToNoMatch={goToNoMatch}
            moveMouse={moveMouse}
          />
        ) : null}
        <div className={styles.working_zone}>
          <TimePade
            time={time}
            scrollRef={scrollRef}
            newBooking={newBooking}
            changeBooking={changeBooking}
            activeTimePade={activeTimePade}
            addBookingFlag={addBookingFlag}
            goToNoMatch={goToNoMatch}
            setTime={setTime}
            newTime={dateTime}
            setNewBooking={setNewBooking}
            setActiveTimePade={setActiveTimePade}
            setAddBookingFlag={setAddBookingFlag}
            setChangeBooking={setChangeBooking}
          />
          <InfoPade
            time={time}
            openModal={openModal}
            newBooking={newBooking}
            activeTimePade={activeTimePade}
            addBookingFlag={addBookingFlag}
            changeBooking={changeBooking}
            setOpenModal={setOpenModal}
            setNewBooking={setNewBooking}
            setActiveTimePade={setActiveTimePade}
            setAddBookingFlag={setAddBookingFlag}
            setChangeBooking={setChangeBooking}
          />
        </div>
      </div>
    </div>
  );
};

export default MainPage;
