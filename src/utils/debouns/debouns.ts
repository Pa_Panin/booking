const debounce = (fn: () => void, ms: number) => {
  let timeout: any;

  return function () {
    const fnCall = () => {
      //@ts-ignore
      fn.apply(this, arguments);
    };

    clearTimeout(timeout);

    timeout = setTimeout(fnCall, ms);
  };
};

export default debounce;
