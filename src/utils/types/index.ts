import moment from "moment";

export type newBookingType = {
  name: string;
  houre: string;
  minute: string;
  duration: string;
};

export type activeTimePadeType = {
  active: string;
  top: string;
  height: string;
};

export type changeBookingType = {
  flag: boolean;
  id: string;
};

export type onHandleClickTimePadeType = {
  index: number;
  position: number;
};

export type newDataToBookingType = {
  newBooking: newBookingType;
  dateTime: moment.Moment;
  time?: number;
  roomName: string;
};

export type stateModalType = {
  flag: boolean;
  id: string;
};

export type postBookingType = {
  chatroom: {
    id: string;
  };
  from: string;
  title: string;
  to: string;
};

export type patchBookingType = {
  data: {
    from: string;
    id: string;
    title: string;
    to: string;
    chatroom: { id: string };
  };
  body: string;
};
