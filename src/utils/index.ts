import React from "react";
import { IBookingType } from "../ducks/booking";
import { newDataToBookingType } from "./types";

export const oneStepInMin = 1.6666667;

export const timeInMinNow = (time: string) => {
  return +time.substr(0, 2) * 60 + +time.substr(2, 2);
};

// export const getFrom

export const gettingBookingDates = (allBooking: IBookingType[]) => {
  return allBooking.map((el) => {
    const from =
      (+el.from.substr(12, 2) * 60 + +el.from.substr(15, 2)) * oneStepInMin;
    const to =
      (+el.to.substr(12, 2) * 60 + +el.to.substr(15, 2)) * oneStepInMin;

    return { from, to, id: el.id };
  });
};

export const newDataToBooking: ({}: newDataToBookingType) => {
  title: string;
  from: string;
  to: string;
} = ({ newBooking, dateTime, time, roomName }) => {
  return {
    title: newBooking.name ? newBooking.name : "New Booking",
    from: dateTime
      .hours(+newBooking.houre)
      .minutes(+newBooking.minute)
      .seconds(0)
      .millisecond(0)
      .toISOString()
      .replace(/(.000)Z$/, "Z"),
    to: dateTime
      .hours(+newBooking.houre)
      .minutes(
        time
          ? +newBooking.minute + time
          : +newBooking.minute + +newBooking.duration
      )
      .seconds(0)
      .millisecond(0)
      .toISOString()
      .replace(/(.000)Z$/, "Z"),
    chatroom: {
      id: roomName,
    },
  };
};
export * from "./debouns";
