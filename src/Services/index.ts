import { API } from "./api";

import { REACT_APP_BASE_URL } from "../config";

import { Office } from "./office";
import { Booking } from "./booking";

export const ApiServices = new API(REACT_APP_BASE_URL || "");

const OfficeServices = new Office(ApiServices);

const BookingSevices = new Booking(ApiServices);

export * from "./api";

export { OfficeServices, BookingSevices };
