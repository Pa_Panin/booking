import { API } from "..";

import { patchBookingType } from "../../utils/types";

export class Booking {
  apiService: API;

  url = "/bookings";
  urlChatroom = "/chatrooms";
  relations = "?relations=bookings";

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  getBooking = (body: { roomName: string }) =>
    this.apiService.queryGET(this.url, body.roomName + this.relations);

  getBookingChatrooms = (body: { roomName: string }) =>
    this.apiService.queryGET(
      this.urlChatroom + "/" + body.roomName + this.relations
    );
  postBooking = (data: object) => this.apiService.queryPOST(this.url, data);

  deleteBooking = (body: { data: string }) =>
    this.apiService.queryDELETE(this.url + `/${body.data}`);

  patchBooking = (data: patchBookingType) =>
    this.apiService.queryPATCH(this.url + `/${data.body}`, data.data);
}
