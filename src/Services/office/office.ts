import { API } from "../api";

export class Office {
  apiService: API;

  url = "/offices";

  constructor(apiServise: API) {
    this.apiService = apiServise;
  }

  getOffice = () => this.apiService.queryGET(this.url);
}
