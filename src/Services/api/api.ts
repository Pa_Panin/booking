import axios from "axios";

enum ERequestsType {
  GET = "GET",
  POST = "POST",
  PATCH = "PATCH",
  DELETE = "DELETE",
}

type QueryTypes = {
  method: ERequestsType;
  url: string;
  header?: object;
  body?: object;
  data?: object;
};

export class API {
  baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  query({ method, url, header, body, data }: QueryTypes) {
    return axios(`${this.baseUrl}${url}`, {
      method,
      headers: {
        "Content-type": "application/json",
        ...header,
      },
      params: body,
      data: data,
    });
  }

  queryGET(url: string, body?: any, header?: object) {
    return this.query({
      method: ERequestsType.GET,
      url,
      body,
      header,
    });
  }

  queryPOST(url: string, data: object, body?: object, header?: object) {
    return this.query({
      method: ERequestsType.POST,
      url,
      header,
      body,
      data,
    });
  }

  queryPATCH(url: string, data: object, body?: object, header?: object) {
    return this.query({
      method: ERequestsType.PATCH,
      url,
      header,
      body,
      data,
    });
  }

  queryDELETE(url: string, header?: object) {
    return this.query({
      method: ERequestsType.DELETE,
      url,
      header,
    });
  }
}
