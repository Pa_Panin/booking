import React from "react";

import classNames from "classnames";

import { EColorTypography, ETypeTypography, Typography } from "..";

import styles from "./styles.module.scss";

export type buttonProps = {
  onHandleClick?: () => void;
  text?: string;
  image?: string;
  buttonType: EbuttonType;
  textColor?: EColorTypography;
};

export enum EbuttonType {
  GREEN_BACKGROUND = "green_background",
  GREEN_BACKGROUND_DARK = "green_background_dark",
  GREEN_BORDER = "green_border",
  RED_BORDER = "red_border",
  WHITE_BORDER = "white_border",
}

const ButtonDefault: React.FC<buttonProps> = ({
  onHandleClick,
  text,
  image,
  buttonType,
  textColor,
}) => {
  return (
    <button
      type="button"
      className={classNames(styles.button, styles[buttonType])}
      onClick={onHandleClick}
    >
      {text ? (
        <Typography
          text={text}
          tag={ETypeTypography.P}
          textColor={textColor ? textColor : undefined}
        />
      ) : null}
      {image ? <img src={image} alt="icon" className={styles.image} /> : null}
    </button>
  );
};

export default ButtonDefault;
