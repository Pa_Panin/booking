import React from "react";

import { ButtonWrapper } from "..";
import { newBookingType } from "../../utils/types";
import { EbuttonType } from "../ButtonDefault/ButtonDefault";

import styles from "./styles.module.scss";

type SelectionBlockType = {
  onHandleClick: () => void;
  onAddNewBooking: () => void;
  opacityRightBlock: boolean;
  newBooking: newBookingType;
};

const SelectionBlock: React.FC<SelectionBlockType> = ({
  newBooking,
  opacityRightBlock,
  onHandleClick,
  onAddNewBooking,
}) => {
  return (
    <div>
      <div className={styles.control_buttons}>
        <div className={styles.control_buttons_options}>
          <ButtonWrapper
            text={"Cancel"}
            buttonType={EbuttonType.RED_BORDER}
            onHandleClick={() => onHandleClick()}
          />
        </div>
        <div className={styles.control_buttons_options}>
          {opacityRightBlock ? (
            <ButtonWrapper
              text={"Go!"}
              buttonType={
                newBooking.name
                  ? EbuttonType.GREEN_BACKGROUND
                  : EbuttonType.GREEN_BACKGROUND_DARK
              }
              onHandleClick={onAddNewBooking}
            />
          ) : (
            <ButtonWrapper
              text={"Ok"}
              buttonType={EbuttonType.GREEN_BACKGROUND}
              onHandleClick={onAddNewBooking}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default SelectionBlock;
