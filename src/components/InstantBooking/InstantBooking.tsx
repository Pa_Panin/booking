import React from "react";

import { ButtonWrapper, ETypeTypography, Typography } from "..";
import { EbuttonType } from "../ButtonDefault/ButtonDefault";

import styles from "./styles.module.scss";

type InstantBookingProps = {
  instantBooking: (arg0: number) => void;
};

const InstantBooking: React.FC<InstantBookingProps> = ({ instantBooking }) => {
  return (
    <div className={styles.time_plus}>
      <div className={styles.time_plus_text}>
        <Typography text="Instant booking" tag={ETypeTypography.H6} />
      </div>
      <div className={styles.time_plus_buttons}>
        <div className={styles.time_plus_buttons_left}>
          <ButtonWrapper
            text={"+10 min"}
            buttonType={EbuttonType.GREEN_BORDER}
            onHandleClick={() => instantBooking(10)}
          />
        </div>
        <div className={styles.time_plus_buttons_right}>
          <ButtonWrapper
            text={"+30 min"}
            buttonType={EbuttonType.GREEN_BORDER}
            onHandleClick={() => instantBooking(30)}
          />
        </div>
      </div>
    </div>
  );
};

export default InstantBooking;
