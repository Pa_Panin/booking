import React from "react";

import { ButtonDefault, EColorTypography } from "../..";
import { EbuttonType } from "../../ButtonDefault/ButtonDefault";

import styles from "./styles.module.scss";

export type linkProps = {
  text: string;
  link: string;
  onHandleClick: () => void;
  buttonType: EbuttonType;
  textColor?: EColorTypography;
};

const LinkWrapper: React.FC<linkProps> = ({
  text,
  onHandleClick,
  link,
  buttonType,
  textColor,
}) => {
  return (
    <a className={styles.link_wrapper} href={link}>
      <ButtonDefault
        text={text}
        onHandleClick={onHandleClick}
        buttonType={buttonType}
        textColor={textColor}
      />
      ;
    </a>
  );
};

export default LinkWrapper;
