import React from "react";

import { buttonProps } from "../../ButtonDefault/ButtonDefault";
import { ButtonDefault } from "../..";

import styles from "./styles.module.scss";

const ButtonWrapper: React.FC<buttonProps> = ({
  text,
  image,
  onHandleClick,
  buttonType,
  textColor,
}) => {
  return (
    <div className={styles.button_wrapper}>
      <ButtonDefault
        text={text}
        image={image}
        onHandleClick={onHandleClick}
        buttonType={buttonType}
        textColor={textColor}
      />
    </div>
  );
};

export default ButtonWrapper;
