import React from "react";

import { ETypeTypography, Typography } from "..";

import styles from "./styles.module.scss";

export type listType = {
  itemArray: {
    name: string;
    id: string;
  }[];
  onClick: (id?: string) => void;
  openFlag: boolean;
  mainName: string;
};

const HeaderList: React.FC<listType> = ({
  itemArray,
  onClick,
  openFlag,
  mainName,
}) => {
  return (
    <div className={styles.visible_office_text} onClick={() => onClick()}>
      <Typography text={mainName} tag={ETypeTypography.P} />
      {itemArray
        .filter((item) => item.name !== mainName)
        .map((el, index) => {
          return el.name !== mainName ? (
            <div
              className={styles.invisible_office_text1}
              style={{
                margin: `${61 * index}px 0 0 0`,
                display: `${openFlag ? `flex` : `none`}`,
              }}
              onClick={() => onClick(el.id)}
              key={el.id}
            >
              <Typography text={el.name} tag={ETypeTypography.P} />
            </div>
          ) : null;
        })}
    </div>
  );
};

export default HeaderList;
