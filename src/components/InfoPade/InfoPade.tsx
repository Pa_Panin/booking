import React, { ChangeEvent, useEffect, useState } from "react";

import { useDispatch, useSelector } from "react-redux";

import { useParams } from "react-router";

import moment from "moment";

import { ButtonWrapper } from "..";
import { Actions, Selectors } from "../../ducks";
import { EbuttonType } from "../ButtonDefault/ButtonDefault";
import {
  gettingBookingDates,
  newDataToBooking,
  oneStepInMin,
  timeInMinNow,
} from "../../utils";
import { Change, Engaged, Static } from "../InfoBlock";
import { ConrolBlock } from "../ControlBlock";
import { InstantBooking } from "../InstantBooking";
import {
  activeTimePadeType,
  changeBookingType,
  newBookingType,
  stateModalType,
} from "../../utils/types";

import styles from "./styles.module.scss";

type InfoTypeProps = {
  time: string;
  newBooking: newBookingType;
  activeTimePade: activeTimePadeType;
  addBookingFlag: boolean;
  changeBooking: changeBookingType;
  openModal: stateModalType;
  setOpenModal: (openModal: stateModalType) => void;
  setNewBooking: (newBooking: newBookingType) => void;
  setAddBookingFlag: (addBookingFlag: boolean) => void;
  setChangeBooking: (changeBooking: changeBookingType) => void;
  setActiveTimePade: (activeTimePade: activeTimePadeType) => void;
};

const InfoPade: React.FC<InfoTypeProps> = ({
  time,
  openModal,
  newBooking,
  activeTimePade,
  addBookingFlag,
  changeBooking,
  setOpenModal,
  setNewBooking,
  setActiveTimePade,
  setAddBookingFlag,
  setChangeBooking,
}) => {
  const dateTime = moment();

  const dispatch = useDispatch();

  const { roomName }: { roomName: string } = useParams();

  const [engagedFlag, setEngagedFlag] = useState({
    flag: false,
    id: "",
    houres: 0,
    min: 0,
  });

  const allBooking = useSelector(Selectors.booking.getAllBooking);

  const timeBooking = [
    10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170,
    180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320,
    340, 350,
  ];

  const hourStart = [
    "00",
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12",
    "13",
    "14",
    "15",
    "16",
    "17",
    "18",
    "19",
    "20",
    "21",
    "22",
    "23",
  ];

  const minStart = ["00", "10", "20", "30", "40", "50"];

  const onAddNewDuration = (event: ChangeEvent<HTMLSelectElement>) => {
    setNewBooking({ ...newBooking, duration: event.target.value });
  };

  useEffect(() => {
    const newTime = timeInMinNow(time) * oneStepInMin;

    if (newTime) {
      const arrayOfPoints = gettingBookingDates(allBooking);

      arrayOfPoints.forEach((el) => {
        if (el.from <= newTime && newTime <= el.to) {
          let timeNew = Math.round((el.to - newTime) / oneStepInMin) + "";
          let houre = 0;
          let min = 0;

          if (+timeNew > 60) {
            houre = Math.trunc(+timeNew / 60);
          }

          min = Math.round(((+timeNew / 60) % 1) * 60);

          setEngagedFlag({
            ...engagedFlag,
            flag: true,
            id: el.id,
            houres: houre,
            min: min,
          });
        } else {
          setEngagedFlag({
            ...engagedFlag,
            flag: false,
            id: el.id,
            houres: 0,
            min: 0,
          });
        }
      });
    }
  }, [allBooking, time]);

  const onAddNewBooking = () => {
    dispatch(
      Actions.booking.postBooking(
        newDataToBooking({ newBooking, dateTime, roomName })
      )
    );

    setActiveTimePade({ ...activeTimePade, active: "none" });
  };

  const onHandleClick = () => {
    setChangeBooking({ ...changeBooking, flag: true });
    setAddBookingFlag(!addBookingFlag);
    setActiveTimePade({ ...activeTimePade, active: "none" });
  };
  const reamain = () => {
    let minHoure = "0";
    let minMinute = "0";

    if (allBooking.length >= 1) {
      minHoure = allBooking[0].from.substr(12, 2);
      minMinute = allBooking[0].from.substr(15, 2);

      allBooking.forEach((el) => {
        let houres = +el.from.substr(12, 2) - +dateTime.format("HH") + "";
        let minutes = +el.from.substr(15, 2) - +dateTime.format("mm") + "";
        let nowTime = +dateTime.format("HH") * 60 + +dateTime.format("mm") + "";
        let nextBooking =
          +el.from.substr(12, 2) * 60 + +el.from.substr(15, 2) + "";

        if (+nextBooking > +nowTime) {
          if (+minutes < 0) {
            houres = +houres - 1 + "";
            minutes = 60 + +minutes + "";
          }
          if (houres === "0") {
            houres = "00";
          }
          if (houres.length === 1) {
            houres = "0" + houres;
          }
          if (minutes.length === 1) {
            minutes = `0${minutes}`;
          }
          if (+minHoure * 60 + +minMinute > +houres * 60 + +minutes) {
            minHoure = houres;
            minMinute = minutes;
          }
        }
      });
    } else {
      minHoure = 23 - +dateTime.format("HH") + "";
      minMinute = 60 - +dateTime.format("mm") + "";
    }
    return `${minHoure}:${minMinute}`;
  };

  const reaminFreeTill = () => {
    let minHoure = "23";
    let minMinute = "59";

    if (allBooking.length >= 1) {
      minHoure = allBooking[0].from.substr(12, 2);
      minMinute = allBooking[0].from.substr(15, 2);
    }

    allBooking.forEach((el, index) => {
      if (el.from) {
        let houres = el.from.substr(12, 2);
        let minutes = el.from.substr(15, 2);

        if (
          +houres * 60 + minMinute >=
          +time.substr(0, 2) * 60 + time.substr(2, 2)
        ) {
          if (houres < minHoure) {
            minHoure = houres;
            minMinute = minutes;
          }
        }
      }
    });

    return `${minHoure}:${minMinute}`;
  };

  const instantBooking = (time: number) => {
    let houres = reamain().substr(0, 2);
    let minutes = reamain().substr(3, 2);

    if (+houres * 60 + +minutes > time) {
      dispatch(
        Actions.booking.postBooking(
          newDataToBooking({ newBooking, dateTime, time, roomName })
        )
      );
    }
  };

  const onAddMinute = (event: ChangeEvent<HTMLSelectElement>) => {
    setNewBooking({
      ...newBooking,
      minute: event?.target.value,
    });

    return newBooking.minute;
  };

  const onAddHoure = (event: ChangeEvent<HTMLSelectElement>) => {
    setNewBooking({
      ...newBooking,
      houre: event?.target.value,
    });

    return newBooking.houre;
  };

  const onAddName = (event: ChangeEvent<HTMLInputElement>) => {
    setNewBooking({ ...newBooking, name: event.target.value });
  };

  const onHandleClickDelete = (id: string) => {
    setOpenModal({ ...openModal, flag: true, id: id });
  };

  const selectBookingTime = () => {
    let selectBookingFrom = "";
    let selectBookingTo = "";

    allBooking.forEach((el) => {
      if (el.id === changeBooking.id) {
        selectBookingFrom = el.from.substr(12, 5);
        selectBookingTo = el.to.substr(12, 5);
      }
    });
    return `${selectBookingFrom} - ${selectBookingTo}`;
  };

  return (
    <div className={styles.info_container}>
      {changeBooking.flag ? (
        <div>
          {engagedFlag.flag ? (
            <Engaged
              engagedFlag={engagedFlag}
              onHandleClickDelete={onHandleClickDelete}
            />
          ) : (
            <Static reaminFreeTill={reaminFreeTill} reamain={reamain} />
          )}
        </div>
      ) : (
        <Change
          allBooking={allBooking}
          changeBooking={changeBooking}
          selectBookingTime={selectBookingTime}
          onHandleClickDelete={onHandleClickDelete}
        ></Change>
      )}
      {addBookingFlag ? (
        <div>
          <div className={styles.control_bar}>
            <div className={styles.add_button}>
              <ButtonWrapper
                text={"Add Booking"}
                buttonType={EbuttonType.GREEN_BACKGROUND}
                onHandleClick={() => onHandleClick()}
              />
            </div>
          </div>
          {changeBooking.flag ? (
            <div>
              {engagedFlag.flag ? null : (
                <InstantBooking instantBooking={instantBooking} />
              )}
            </div>
          ) : null}
        </div>
      ) : (
        <div>
          {changeBooking.flag ? (
            <ConrolBlock
              onAddName={onAddName}
              onAddHoure={onAddHoure}
              onAddMinute={onAddMinute}
              onAddNewDuration={onAddNewDuration}
              onAddNewBooking={onAddNewBooking}
              onHandleClick={onHandleClick}
              newBooking={newBooking}
              timeBooking={timeBooking}
              hourStart={hourStart}
              minStart={minStart}
            />
          ) : null}
        </div>
      )}
    </div>
  );
};

export default InfoPade;
