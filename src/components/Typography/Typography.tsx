import React from "react";

import classNames from "classnames";

import styles from "./styles.module.scss";

type Props = {
  text: string;
  tag: ETypeTypography;
  textColor?: EColorTypography;
};

export enum ETypeTypography {
  H1 = "h1",
  H2 = "h2",
  H3 = "h3",
  H4 = "h4",
  H5 = "h5",
  H6 = "h6",
  H7 = "h7",
  P = "p",
}

export enum EColorTypography {
  WHITE = "white",
  BLACK = "black",
  RED = "red",
  WHITE50 = "white50",
}

export const Typography: React.FC<Props> = ({
  text,
  tag,
  textColor = "white",
}) => (
  <div
    className={classNames(styles.base_class, styles[tag], styles[textColor])}
  >
    {text}
  </div>
);
