import React from "react";

import {
  ButtonWrapper,
  EColorTypography,
  ETypeTypography,
  Typography,
} from "..";
import { EbuttonType } from "../ButtonDefault/ButtonDefault";

import styles from "./styles.module.scss";

type EngagedProps = {
  engagedFlag: {
    flag: boolean;
    id: string;
    houres: number;
    min: number;
  };
  onHandleClickDelete: (id: string) => void;
};

const Engaged: React.FC<EngagedProps> = ({
  engagedFlag,
  onHandleClickDelete,
}) => {
  return (
    <div>
      <div className={styles.engaged_container}>
        <div>
          <div className={styles.title}>
            <Typography
              text={"Title"}
              textColor={EColorTypography.WHITE}
              tag={ETypeTypography.H4}
            />
          </div>
          <div className={styles.status}>
            <Typography
              text={"Engaged"}
              textColor={EColorTypography.WHITE}
              tag={ETypeTypography.H3}
            />
          </div>
          <div className={styles.ramin}>
            <Typography
              text={"Remain"}
              textColor={EColorTypography.WHITE}
              tag={ETypeTypography.H4}
            />
          </div>
          <div className={styles.time}>
            <Typography
              text={`${engagedFlag.houres + "h" + engagedFlag.min + "min"}`}
              textColor={EColorTypography.WHITE}
              tag={ETypeTypography.H3}
            />
          </div>
        </div>
        <div className={styles.button_panel}>
          <div className={styles.button_change}>
            <ButtonWrapper
              buttonType={EbuttonType.WHITE_BORDER}
              text={"Booking Right After"}
            />
          </div>
          <div className={styles.button_delete}>
            <ButtonWrapper
              buttonType={EbuttonType.WHITE_BORDER}
              text={"STOP"}
              onHandleClick={() => onHandleClickDelete(engagedFlag.id)}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Engaged;
