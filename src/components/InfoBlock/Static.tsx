import React from "react";

import { ETypeTypography, Typography } from "..";

import styles from "./styles.module.scss";

type StaticProps = {
  reaminFreeTill: () => string;
  reamain: () => string;
};

const Static: React.FC<StaticProps> = ({ reaminFreeTill, reamain }) => {
  return (
    <div>
      <div className={styles.info_bar}>
        <div className={styles.info}>
          <div className={styles.info_card_top}>
            <Typography text={"Free till"} tag={ETypeTypography.H4} />
            <Typography text={reaminFreeTill()} tag={ETypeTypography.H1} />
          </div>
          <div className={styles.info_card_bottom}>
            <Typography text={"Remain"} tag={ETypeTypography.H4} />
            <Typography text={reamain()} tag={ETypeTypography.H2} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Static;
