import React from "react";

import { ButtonWrapper, ETypeTypography, Typography } from "..";
import { changeBookingType } from "../../utils/types";
import { EbuttonType } from "../ButtonDefault/ButtonDefault";
import { IBookingType } from "../../ducks/booking";

import { delete as deleteBooking } from "../../assets";

import styles from "./styles.module.scss";

type ChangeProps = {
  onHandleClickDelete: (id: string) => void;
  selectBookingTime: () => string;
  changeBooking: changeBookingType;
  allBooking: IBookingType[];
};

const Change: React.FC<ChangeProps> = ({
  allBooking,
  changeBooking,
  selectBookingTime,
  onHandleClickDelete,
}) => {
  return (
    <div>
      {allBooking.length !== 0 ? (
        <div className={styles.info_bar_booking}>
          <div className={styles.info_booking}>
            <div className={styles.info_card_booking_top}>
              <Typography text={"Title"} tag={ETypeTypography.P} />
              <div style={{ margin: "13px 0 0 0" }}>
                <Typography
                  text={
                    allBooking.find((el) => el.id === changeBooking.id)!.title
                  }
                  tag={ETypeTypography.H3}
                />
              </div>
            </div>
            <div className={styles.info_card_booking_bottom}>
              <Typography text={"Timing"} tag={ETypeTypography.P} />
              <div style={{ margin: "13px 0 0 0" }}>
                <Typography
                  text={selectBookingTime()}
                  tag={ETypeTypography.H3}
                />
              </div>
            </div>
          </div>
          <div className={styles.info_buttons_panel}>
            <div className={styles.right_after}>
              <ButtonWrapper
                buttonType={EbuttonType.GREEN_BORDER}
                text={"Booking Right After"}
              />
            </div>
            <div className={styles.delete}>
              <ButtonWrapper
                buttonType={EbuttonType.RED_BORDER}
                image={deleteBooking}
                onHandleClick={() => onHandleClickDelete(changeBooking.id)}
              />
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default Change;
