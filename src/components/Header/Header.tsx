import React, { useCallback, useState } from "react";

import Moment from "react-moment";
import { useHistory, useParams } from "react-router";

import { HeaderList } from "../HeaderList";

import { calendar, logo } from "../../assets";

import styles from "./styles.module.scss";

type HeaderProps = {
  officeProps: HeaderOfficeType;
  goToNoMatch: () => void;
  moveMouse: () => void;
};

export type HeaderOfficeType = {
  name: string;
  id: string;
  chatrooms: {
    id: string;
    name: string;
    createdAt: string;
    updatedAt: string;
  }[];
}[];

const Header: React.FC<HeaderProps> = ({
  officeProps,
  goToNoMatch,
  moveMouse,
}) => {
  const history = useHistory();

  const { roomName }: { roomName: string } = useParams();
  const { officeName }: { officeName: string } = useParams();

  const [roomFlag, setRoomFlag] = useState(false);
  const [officeFlag, setOfficeFlag] = useState(false);

  const handleClickOnItemOffice = (id?: string) => {
    setOfficeFlag(!officeFlag);
    setRoomFlag(false);

    if (id) {
      const newNameOffice = officeProps.find((el) => el.id === id)!.id;

      history.replace(
        `/office/${newNameOffice}/room/${
          officeProps.find((el) => el.id === newNameOffice)?.chatrooms[0].id
        }`
      );
      moveMouse();
    }
  };

  const handleClickOnItemRoom = (id?: string) => {
    setRoomFlag(!roomFlag);
    setOfficeFlag(false);

    if (id) {
      history.replace(
        `/office/${officeName}/room/${
          officeProps
            .find((el) => el.id === officeName)
            ?.chatrooms.find((el) => el.id === id)!.id
        }`
      );
      moveMouse();
    }
  };

  const mainNameOffice = () => {
    const el = officeProps.find((el) => el.id === officeName);

    if (!el?.name) {
      goToNoMatch();

      return "";
    } else {
      return el.name;
    }
  };

  const mainNameRoom = () => {
    if (officeProps.find((el) => el.id === officeName) !== undefined) {
      return officeProps
        .find((el) => el.id === officeName)!
        .chatrooms.find((el) => el.id === roomName)!.name;
    } else {
      goToNoMatch();

      return "";
    }
  };

  const itemArrayOffice = useCallback(() => {
    return officeProps.map((el) => {
      return { name: el.name, id: el.id };
    });
  }, [officeProps]);

  const itemArrayRoom = useCallback(() => {
    return officeProps
      .filter((el) => el.id === officeName)
      .flatMap((el) =>
        el.chatrooms.map((item) => {
          return { name: item.name, id: item.id };
        })
      );
  }, [officeName, officeProps]);

  return (
    <div className={styles.header}>
      <div className={styles.header_logo}>
        <img src={logo} alt={`Logotype`}></img>
      </div>
      <div className={styles.header_panel_office}>
        <HeaderList
          onClick={handleClickOnItemOffice}
          openFlag={officeFlag}
          mainName={mainNameOffice()}
          itemArray={itemArrayOffice()}
        />
      </div>
      <div className={styles.header_panel_room}>
        <HeaderList
          onClick={handleClickOnItemRoom}
          openFlag={roomFlag}
          mainName={mainNameRoom()}
          itemArray={itemArrayRoom()}
        />
      </div>
      <div className={styles.header_panel_date}>
        <Moment format="D MMMM HH:mm" interval={1000} />
      </div>
      <div className={styles.header_panel_calendar}>
        <img src={calendar} alt={`Calendar`}></img>
      </div>
    </div>
  );
};

export default Header;
