import React, { RefObject, useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";

import { useParams } from "react-router";

import moment from "moment";

import { ETypeTypography, Typography } from "..";
import { Actions, Selectors } from "../../ducks";
import { gettingBookingDates, oneStepInMin, timeInMinNow } from "../../utils";
import { BookingTime } from "./BookingTime";
import { PastTense } from "./PastTense";
import {
  activeTimePadeType,
  changeBookingType,
  newBookingType,
  onHandleClickTimePadeType,
} from "../../utils/types";

import styles from "./styles.module.scss";

type PropsTimePade = {
  time: string;
  scrollRef: RefObject<HTMLDivElement>;
  addBookingFlag: boolean;
  newBooking: newBookingType;
  changeBooking: changeBookingType;
  activeTimePade: activeTimePadeType;
  goToNoMatch: () => void;
  newTime: () => moment.Moment;
  setTime: (time: string) => void;
  setNewBooking: (newBooking: newBookingType) => void;
  setAddBookingFlag: (addBookingFlag: boolean) => void;
  setActiveTimePade: (activeTimePade: activeTimePadeType) => void;
  setChangeBooking: (changeBooking: changeBookingType) => void;
};

let height = 0;
let duration = 0;
let top = 0;
let hours = 0;
let minute = 0;
let active = "block";
const rightPadeArray = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
  22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
  41, 42, 43, 44, 45, 46, 47,
];

const leftPadeArray = [
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
  22, 23,
];

const TimePade: React.FC<PropsTimePade> = ({
  time,
  scrollRef,
  newBooking,
  changeBooking,
  activeTimePade,
  addBookingFlag,
  goToNoMatch,
  newTime,
  setTime,
  setNewBooking,
  setActiveTimePade,
  setAddBookingFlag,
  setChangeBooking,
}) => {
  const { roomName }: { roomName: string } = useParams();
  const allBooking = useSelector(Selectors.booking.getAllBooking);

  const dispatch = useDispatch();

  const getBookingAPI = () => {
    dispatch(
      Actions.booking.getBookingChatrooms({
        roomName: roomName,
        reject: () => goToNoMatch(),
      })
    );
  };

  const patchBookingApi = () => {
    // const data = {
    //   data: {
    //     from: "2021-11-10T13:01:00Z",
    //     id: "f619c18f-ff55-4638-9b57-7893f1197745",
    //     title: "New Booking",
    //     to: "2021-11-10T14:00:00Z",
    //     chatroom: { id: "249e815b-b2cc-47cf-a77b-9c181cf15f40" },
    //   },
    //   body: "e6629ffb-a0e8-4cd0-8963-1245c39ed98e",
    // };
    // dispatch(Actions.booking.patchBooking(data));
  };

  const updateTime = () => {
    const dateTime = newTime();
    setTime(dateTime.format("HHmm"));
  };

  const onHandleClickTimePade = ({
    index,
    position,
  }: onHandleClickTimePadeType) => {
    const i = index / 2;
    let setMin;
    let setHoure = Math.floor(i);

    setAddBookingFlag(false);
    setChangeBooking({ ...changeBooking, flag: true });

    if (Math.round(i) === i) {
      switch (position) {
        case 1:
          setMin = 0;
          break;
        case 2:
          setMin = 10;
          break;
        case 3:
          setMin = 20;
          break;
        default:
          setMin = 0;
      }
    } else {
      switch (position) {
        case 1:
          setMin = 30;
          break;
        case 2:
          setMin = 40;
          break;
        case 3:
          setMin = 50;
          break;
        default:
          setMin = 0;
      }
    }
    hours = setHoure;
    height = 100;
    minute = setMin;
    top = (+setHoure * 60 + setMin) * oneStepInMin;

    bookingBoundaries();
    setToState();
  };

  const setToState = () => {
    if (hours.toString().length < 2) {
      setNewBooking({
        ...newBooking,
        houre: "0" + hours,
        minute: minute + "",
        duration: duration + "",
      });
    } else {
      setNewBooking({
        ...newBooking,
        houre: hours + "",
        minute: minute + "",
        duration: duration + "",
      });
    }
    setActiveTimePade({
      ...activeTimePade,
      active: "block",
      top: top + "",
      height: height + "",
    });
  };

  const bookingBoundaries = () => {
    const timePadeNow = top + height;

    if (timePadeNow > 2384) {
      // Бронирование дольше чем 23:50
      height = 100 - (timePadeNow - 2382.9);
      active = "block";
    }

    if (allBooking.length >= 1) {
      // Другие бронирования есть

      const arrayOfPoints = gettingBookingDates(allBooking);

      active = "block";

      arrayOfPoints.forEach((el) => {
        if (top >= 2382) {
          active = "none";
          height = 32;
          top = 0;
          setAddBookingFlag(true);
        }
        if (active !== "none") {
          if (top > +time) {
            if (top >= el.from && top < el.to) {
              // Новое бронирование находится внутри имеющегося бронирования

              active = "none";
            } else if (
              (top < el.from && top + height > el.to) ||
              (top < el.from && top + height > el.from)
            ) {
              // Новое пробнирование заезжает на одно из имеющихся
              height = height - (top + height - el.from);
              active = "block";
            } else if (top >= el.from && top < el.to) {
              active = "none";
            } else {
            }
          } else {
            active = "none";
          }
        }
      });
    }

    duration = Math.round(height / oneStepInMin);
  };

  useEffect(() => {
    top = (+newBooking.houre * 60 + +newBooking.minute) * oneStepInMin;
    height = +newBooking.duration * oneStepInMin;

    bookingBoundaries();

    if (!addBookingFlag) {
      setActiveTimePade({
        ...activeTimePade,
        active: active,
        top: top + "",
        height: height + "",
      });
    }

    if (+newBooking.duration !== duration) {
      setNewBooking({
        ...newBooking,

        duration: duration + "",
      });
    }
  }, [newBooking, addBookingFlag]);

  useEffect(() => {
    getBookingAPI();
  }, [roomName]);

  useEffect(() => {
    setInterval(updateTime, 2000);
  }, []);

  return (
    <div className={styles.time_pade} ref={scrollRef}>
      <div className={styles.left_bar}>
        {leftPadeArray.map((el, index) => (
          <div className={styles.left_block} key={index}>
            <div className={styles.line_1}>
              <div className={styles.line_time}>
                <Typography text={`${el}:00`} tag={ETypeTypography.H4} />
              </div>
            </div>
            <div className={styles.line_2} />
            <div className={styles.line_3} />
            <div className={styles.line_4} />
            <div className={styles.line_5} />
            <div className={styles.line_6} />
            {index === 23 ? (
              <div className={styles.line_7}>
                <div className={styles.line_time}>
                  <Typography text={`${el + 1}:00`} tag={ETypeTypography.H4} />
                </div>
              </div>
            ) : null}
          </div>
        ))}
      </div>
      <div className={styles.right_bar}>
        <div
          className={styles.activeBooking}
          style={{
            top: `${activeTimePade.top}px`,
            display: `${activeTimePade.active}`,
            height: `${activeTimePade.height}px`,
          }}
        >
          <div>{newBooking.name}</div>
        </div>
        {allBooking.map((el) => (
          <BookingTime
            bookingData={{
              title: el.title,
              startTime: el.from,
              endTime: el.to,
            }}
            handleClickBooking={() =>
              setChangeBooking({
                ...changeBooking,
                flag: false,
                id: el.id,
              })
            }
          />
        ))}
        <div
          className={styles.past_time}
          style={{
            height: `${timeInMinNow(time) * 0.06944444}%`,
          }}
        >
          <PastTense />
          <div className={styles.past_time_line} />
        </div>
        {rightPadeArray.map((el, index) => (
          <div className={styles.right_block} key={index}>
            <div
              className={styles.target_block}
              onClick={() =>
                onHandleClickTimePade({
                  index: index,
                  position: 1,
                })
              }
            />
            <div
              className={styles.target_block}
              onClick={() =>
                onHandleClickTimePade({
                  index: index,
                  position: 2,
                })
              }
            />
            {index === 47 ? (
              <div className={styles.not_target_block} />
            ) : (
              <div
                className={styles.target_block}
                onClick={() =>
                  onHandleClickTimePade({
                    index: index,
                    position: 3,
                  })
                }
              />
            )}
          </div>
        ))}
        <div className={styles.bottom_right_line} />
        <div className={styles.fake_right_block} />
      </div>
    </div>
  );
};

export default TimePade;
