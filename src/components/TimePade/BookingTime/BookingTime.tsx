import React from "react";

import { ETypeTypography, Typography } from "../..";

import styles from "./styles.module.scss";

type bookingTimeProps = {
  handleClickBooking: () => void;
  bookingData: {
    title: string;
    startTime: string;
    endTime: string;
  };
};

const BookingTime: React.FC<bookingTimeProps> = ({
  bookingData,
  handleClickBooking,
}) => {
  const extractionDataStart = (data: string) => {
    return +data.substr(12, 2) * 60 + +data.substr(15, 2);
  };
  return (
    <div
      className={styles.booking_time}
      style={{
        top: `${extractionDataStart(bookingData.startTime) * 0.06944444}%`,
        height: `${
          (extractionDataStart(bookingData.endTime) -
            extractionDataStart(bookingData.startTime)) *
          0.06944444
        }%`,
      }}
      onClick={handleClickBooking}
    >
      <Typography text={bookingData.title} tag={ETypeTypography.H5} />
    </div>
  );
};

export default BookingTime;
