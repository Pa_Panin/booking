import React, { ChangeEvent } from "react";

import { EColorTypography, ETypeTypography, Typography } from "..";
import { SelectionBlock } from "../SelectionBlock";
import { newBookingType } from "../../utils/types";

import styles from "./styles.module.scss";

type ControlBlockProps = {
  onAddName: (event: ChangeEvent<HTMLInputElement>) => void;
  onAddHoure: (event: ChangeEvent<HTMLSelectElement>) => void;
  onAddMinute: (event: ChangeEvent<HTMLSelectElement>) => void;
  onAddNewDuration: (event: ChangeEvent<HTMLSelectElement>) => void;
  onAddNewBooking: () => void;
  onHandleClick: () => void;
  newBooking: newBookingType;
  timeBooking: number[];
  hourStart: string[];
  minStart: string[];
};
const ControlBlock: React.FC<ControlBlockProps> = ({
  onAddName,
  onAddHoure,
  onAddMinute,
  onAddNewDuration,
  onAddNewBooking,
  onHandleClick,
  newBooking,
  timeBooking,
  hourStart,
  minStart,
}) => {
  return (
    <div className={styles.control_add_booking}>
      <div className={styles.control_top_content}>
        <div className={styles.control_top_content_text}>
          <Typography
            text="Title"
            tag={ETypeTypography.H6}
            textColor={EColorTypography.WHITE50}
          />
        </div>
        <input
          className={styles.control_top_content_input}
          placeholder={"New Booking"}
          onChange={onAddName}
          value={newBooking.name}
        />
      </div>
      <div className={styles.control_bottom_content}>
        <div className={styles.control_time}>
          <div className={styles.control_time_text}>
            <div>
              <Typography
                text="Start time"
                tag={ETypeTypography.H6}
                textColor={EColorTypography.WHITE50}
              />
            </div>
            <div className={styles.control_time_houre}>
              <select
                className={styles.control_time_input}
                name="hourStart"
                value={newBooking.houre}
                onChange={onAddHoure}
              >
                {hourStart.map((el) => (
                  <option value={el} key={el}>
                    {el}
                  </option>
                ))}
              </select>
              <div className={styles.control_time_houre_space}>
                <Typography
                  text=":"
                  tag={ETypeTypography.H4}
                  textColor={EColorTypography.WHITE50}
                />
              </div>
              <select
                className={styles.control_time_input}
                name="minStart"
                value={+newBooking.minute}
                onChange={onAddMinute}
              >
                {minStart.map((el) => (
                  <option value={el} key={el}>
                    {el}
                  </option>
                ))}
              </select>
            </div>
          </div>
          <div className={styles.control_time_min}>
            <div className={styles.control_time_min_container}>
              <div>
                <Typography
                  text="Duration"
                  tag={ETypeTypography.H6}
                  textColor={EColorTypography.WHITE50}
                />
              </div>
              <div className={styles.control_time_min_container_bottom}>
                <div>
                  <select
                    className={styles.control_time_input}
                    name="timeBooking"
                    value={newBooking.duration}
                    onChange={onAddNewDuration}
                  >
                    {timeBooking.map((el) => (
                      <option value={el} key={el}>
                        {el}
                      </option>
                    ))}
                  </select>
                </div>
                <div className={styles.control_time_min_container_bottom_text}>
                  <Typography text="min" tag={ETypeTypography.P} />
                </div>
              </div>
            </div>
          </div>
        </div>
        <SelectionBlock
          newBooking={newBooking}
          opacityRightBlock={true}
          onHandleClick={onHandleClick}
          onAddNewBooking={onAddNewBooking}
        />
      </div>
    </div>
  );
};

export default ControlBlock;
