import React from "react";

import { useDispatch } from "react-redux";

import { SelectionBlock } from "../SelectionBlock";
import { ETypeTypography, Typography } from "..";
import { Actions } from "../../ducks";
import {
  changeBookingType,
  newBookingType,
  stateModalType,
} from "../../utils/types";

import styles from "./styles.module.scss";

type ModalProps = {
  openModal: stateModalType;
  newBooking: newBookingType;
  changeBooking: changeBookingType;
  setOpenModal: (openModal: stateModalType) => void;
  setChangeBooking: (changeBooking: changeBookingType) => void;
};

const Modal: React.FC<ModalProps> = ({
  openModal,
  newBooking,
  changeBooking,
  setOpenModal,
  setChangeBooking,
}) => {
  const dispatch = useDispatch();

  const onHandleClick = () => {
    setOpenModal({ ...openModal, flag: false });
  };

  const onHandleClickDelete = (id: string) => {
    dispatch(Actions.booking.deleteBooking(id));
    setOpenModal({ ...openModal, flag: false });
    setChangeBooking({ ...changeBooking, flag: true });
  };

  if (openModal.flag) {
    return (
      <div className={styles.modal_container}>
        <div className={styles.booking}>
          <div>
            <Typography
              text="Are you sure you want to
                    delete this booking?"
              tag={ETypeTypography.H3}
            />
          </div>
          <div className={styles.control_bar}>
            <div className={styles.control_bar_container}>
              <SelectionBlock
                opacityRightBlock={false}
                newBooking={newBooking}
                onHandleClick={onHandleClick}
                onAddNewBooking={() => onHandleClickDelete(openModal.id)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
};

export default Modal;
